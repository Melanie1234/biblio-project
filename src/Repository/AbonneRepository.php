<?php


namespace App\Repository;

use App\Entities\Abonne;
use PDO;

class AbonneRepository{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $abonnes = [];
        $statement = $this->connection->prepare('SELECT * FROM abonne');
        $statement->execute();

        foreach ($statement->fetchAll() as $line){
            $abonnes[] = new Abonne($line['nom'], $line['prenom'], $line['id']);
        }
        return $abonnes;
    }

    public function persist(Abonne $abonne) {
        $statement = $this->connection->prepare('INSERT INTO abonne (nom,prenom) VALUES (:nom, :prenom)');
        $statement->bindValue('nom', $abonne->getNom());
        $statement->bindValue('prenom', $abonne->getPrenom());


        $statement->execute();

        $abonne->setId($this->connection->lastInsertId());

    }
    public function update(Abonne $abonne)
    {
        $statement = $this->connection->prepare('UPDATE abonne SET nom = :nom, prenom = :prenom WHERE id = :id');
        $statement->bindValue('nom', $abonne->getNom());
        $statement->bindValue('prenom', $abonne->getPrenom());
        $statement->bindValue('id', $abonne->getId());

        $statement->execute();

    }

}
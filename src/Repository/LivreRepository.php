<?php

namespace App\Repository;

use App\Entities\Livre;
use DateTime;
use PDO;

class LivreRepository{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $livres = [];
        $statement = $this->connection->prepare('SELECT * FROM livre');
        $statement->execute();

        foreach ($statement->fetchAll() as $line){
            $livres[] = $this->sqlToLivre($line);
        }
        return $livres;
    }
 private function sqlToLivre(array $line):Livre {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
    
        return new Livre($line['titre'], $line['auteur'], $date, $line['dispo'], $line['id']);
    }

    public function persist(Livre $livre) {
        $statement = $this->connection->prepare('INSERT INTO livre (titre,auteur, date, dispo) VALUES (:titre, :auteur, :date, :dispo)');
        $statement->bindValue('titre', $livre->getTitre());
        $statement->bindValue('auteur', $livre->getAuteur());
        $statement->bindValue('date', $livre->getDate()->format('Y-m-d'));
        $statement->bindValue('dispo', $livre->getDispo());

        $statement->execute();

        $livre->setId($this->connection->lastInsertId());
    }

}